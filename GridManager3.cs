﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Works only with ratio 2:1 =(

public class GridManager3 : MonoBehaviour {
  private int [,] grid;
  public Sprite sprite;
  private int horizontal, vertical, columns, rows;

  private void Start() {
    CreateGrid();
  }

  private void CreateGrid() {
    vertical = (int)Camera.main.orthographicSize;
    horizontal = vertical * (Screen.width / Screen.height);

    columns = horizontal * 2;
    rows = vertical * 2;

    grid = new int[columns, rows];

    Debug.Log($"vertical: {vertical} horizontal: {horizontal}");

    for (int columnsCount = 0, masterIndex = 0; columnsCount < columns; columnsCount++) {
      for (int rowsCount = 0; rowsCount < rows; rowsCount++) {
        grid[columnsCount, rowsCount] = masterIndex;
        masterIndex++;

        SpawnTile(columnsCount, rowsCount, grid[columnsCount, rowsCount]);
      }
    }    
  }

  private void SpawnTile(int x, int y, int value) {
    GameObject gameObject = new GameObject($"x: {x} y: {y} value: {value}");
    gameObject.transform.position = new Vector3(x - (horizontal - 0.5f), y - (vertical - 0.5f), 0);

    if (sprite) {
      SpriteRenderer SpriteRendererComponent = gameObject.AddComponent<SpriteRenderer>();
      SpriteRendererComponent.sprite = sprite;
      SpriteRendererComponent.color = Random.ColorHSV();
    } else {
      Debug.LogError("sprite is not exists");
    }
  }
}
