﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour {
  public Vector2 startPosition = new Vector2(0, 0);
  public int columns;
  public int rows;
  public int xSpace = 1, ySpace = 1;
  public GameObject prefab;
  private Vector2 _startPosition = new Vector2(0, 0);
  private SpriteRenderer prefabSpriteRenderer;
    
  void Start() {
    CalculateStartPosition();
    CreateGrid();
  } 

  private void CreateGrid() {
    for (int i = 0; i < columns * rows; i++) {
      Vector3 position = new Vector3(_startPosition.x + (xSpace * (i % columns)), _startPosition.y - (-ySpace * (i / columns)), 0);
      Instantiate(prefab, position, Quaternion.identity);
    }
  }

  private void CalculateStartPosition() {
    prefabSpriteRenderer =  prefab.GetComponent<SpriteRenderer>();

    if (prefabSpriteRenderer != null) {
      _startPosition = new Vector3(startPosition.x, startPosition.y, 0) + prefabSpriteRenderer.bounds.extents;
    } else {
      Debug.LogError("SpriteRenderer not found");
    }
  }
}
