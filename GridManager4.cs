﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager4 : MonoBehaviour {
 [SerializeField] private int rows = 5, cols = 8;
 [SerializeField] private float tileSize = 1;

 [SerializeField] private GameObject referenceTile;


  private void Start() {
    if (referenceTile) {
      GenerateGrid();
    } else {
      Debug.LogError("referenceTile is not exists");
    }
  }

  private void GenerateGrid() {
    for (int col = 0; col < cols; col++) {
      for (int row = 0; row < rows; row++) {
        GameObject tile = (GameObject)Instantiate(referenceTile, transform);

        float positionX = col * tileSize;
        float positionY = row * tileSize;

        tile.transform.position = new Vector3(positionX + tileSize / 2, positionY + tileSize / 2, 0);
        Utilities.CreateWorldTextMesh($"{row}{col}", new Vector3(positionX + tileSize / 2, positionY + tileSize / 2, 0), 8);
      }
    }
  }
}
