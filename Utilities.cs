﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utilities {
  /**
    * Создание и вывод на экран текста при помощи TextMesh
    * @param {string} text 
    * @param {Vector3} localPosition 
    * @param {int} fontSize 
    * @return {TextMesh} TextMesh object
    **/
  public static TextMesh CreateWorldTextMesh(string text, Vector3 localPosition = default(Vector3), int fontSize = 40) {
    GameObject gameObject = new GameObject("World_Text", typeof(TextMesh));
    Transform transform = gameObject.transform;
    TextMesh textMesh = gameObject.GetComponent<TextMesh>();

    transform.localPosition = localPosition;
    textMesh.anchor = TextAnchor.MiddleCenter;
    textMesh.alignment = TextAlignment.Center;
    textMesh.text = text;
    textMesh.fontSize = fontSize;
    textMesh.color = Color.black;
  
    return textMesh;
  }
}
